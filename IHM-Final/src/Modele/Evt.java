package Modele;


public class Evt
{
	private Date chDate ;
	private String chTitre, chLieu ;
	private static int chNbr ;
	private String chHeureDeb, chMDeb, debut1,debut2,fin1,fin2;
	
	public Evt (Date parDate, String parTitre, String parLieu,String parDebut1,String parDebut2,String parFin1,String parFin2)
	{
		chDate = parDate ;
		chTitre = parTitre ;
		chLieu = parLieu ;
		chNbr++ ;
		debut1 = parDebut1;
		debut2 = parDebut2;
		fin1 = parFin1;
		fin2 = parFin2;
	}
	
	public String toString()
	{
		return (chTitre + " le " + chDate.toString() + " a " + chLieu );
	}
	
	public Date getDate()
	{
		return chDate ;
	}
	
	public String getTitre()
	{
		return chTitre ;
	}
	
	public String getLieu()
	{
		return chLieu ;
	}
	
	public int getNbr()
	{
		return chNbr ;
	}
	
	public void setDate(Date parDate)
	{
		chDate = parDate ;
	}
	
	public void setTitre(String parTitre)
	{
		chTitre = parTitre ;
	}
	
	public void setLieu(String parLieu)
	{
		chLieu = parLieu ;
	}
	
	/*public static Evt lireEvt()
	{
		System.out.println("Entrez la date, le titre et le lieu de l'evenement") ;
		Date date = Date.lireDate() ;
		String titre = Clavier.lireString() ;
		String lieu = Clavier.lireString() ;
		return new Evt(date,titre,lieu) ;
	}*/
	
	public int compareTo(Evt parEvt)
	{
		if (chDate.compareTo(parEvt.chDate) == 0)
		{
			return (chTitre.compareTo(parEvt.chTitre) );
		}
		return chDate.compareTo(parEvt.chDate) ;
	}
}