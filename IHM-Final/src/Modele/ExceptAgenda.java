package Modele;

public class ExceptAgenda extends Exception //Heritage
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExceptAgenda (String parMsg)
	{
		super(parMsg); // appeler le constructeur de classe mere permettant de recuperer le msg via getter getMessage() d'Exception
	}
}