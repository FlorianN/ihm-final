package Vue;


import java.awt.*;
import javax.swing.*;
import java.awt.Insets;
import Modele.*;

public class FenetreMere extends JFrame  {
	
	public FenetreMere(String parTitre,Agenda parAgenda) {
		
		super(parTitre);
		PanelFils contentPane = new PanelFils(parAgenda);
		setContentPane (contentPane);
		contentPane.setBackground (Color.white);
		setDefaultCloseOperation (EXIT_ON_CLOSE);
		setSize(600, 400);
		pack();
		setVisible(true);
		setLocation(200, 300);
	}
	
	public static void main (String [] args){
		Agenda a  = new Agenda();
		FenetreMere fenetre = new FenetreMere("Calendrier 2017", a);
	}
	//public Insets getInsets(){
		//return new Insets(15,15,15,15);
	//}
}


