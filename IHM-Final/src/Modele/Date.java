package Modele;


import java.util.* ;

public class Date
{
	private int chJour,chMois,chAn ; // attribut/champ
	 private int chJourSemaine ; // dimache = 1, lundi = 2
	
	public Date (int parJour, int parMois, int parAn) /*throws ExceptAgenda*/ // constructeur
	{
		chJour = parJour ;
		chMois = parMois ;
		chAn = parAn ;
		/*if (chJour < 1 || chJour > dernierJourDuMois(parMois,parAn) ) 
		{
			throw new ExceptAgenda("Jour invalide") ;
		}
		if (chMois < 1 || chMois > 12) 
		{
			throw new ExceptAgenda("Mois invalide") ;
		}
		if (chAn < 1582)
		{
			throw new ExceptAgenda("Annee invalide") ;
		}*/
	}
	
	public Date() // constructeur par défaut
	{
		GregorianCalendar dateAuj = new GregorianCalendar() ;
		chAn = dateAuj.get(Calendar.YEAR) ;
		chMois = dateAuj.get(Calendar.MONTH)+1 ; //janvier = 0, février = 1
		chJour = dateAuj.get(Calendar.DAY_OF_MONTH) ;
		chJourSemaine = dateAuj.get(Calendar.DAY_OF_WEEK) ;
		
	}
	
	public String toString() // methode
	{
		return (chJour + "/" + chMois + "/" + chAn) ;
	}
	
	public static int dernierJourDuMois(int parMois, int parAn) // methode
	{
		switch(parMois)
		{
			case 4:
			case 6:
			case 9:
			case 11:
				return 30 ;
			case 2:
				if ((parAn % 4 == 0 && parAn % 100 != 0) || (parAn % 400 == 0))
					return 29 ;
				else
					return 28 ;
			default:
				return 31 ;
		}
	}
	

	
	public boolean estValide() // methode
	{
		if (chAn < 1582)
		{
			return false ;
		}
		else
		{
			if (chMois > 13)
			{
				return false ;
			}
			else
			{
				if (chMois < 0)
				{
					return false ;
				}
				else
				{
					if (chJour >= dernierJourDuMois(chMois,chAn) )
					{
						return false ;
					}
					else
					{
						return true ;
					}
				}
			}
		}
	} 
	
	/*public static Date lireDate() throws Exception // methode
	{
		try {
			System.out.println("Entrez jour,mois,annee") ;
		int jour = Clavier.lireInt() ;
		int mois = Clavier.lireInt() ;
		int annee = Clavier.lireInt() ;
		}
		catch (ExceptAgenda ex) {
			return null ;
			System.err.println("Erreur : " + ex.getMessage() ) ;
			System.exit(-1) ;
		}
		return new Date(jour,mois,annee) ;
	} */
	
	public int compareTo(Date parDate) // methode
	{
		if (chAn < parDate.chAn)
		{
			return -1 ;
		}
		else if (chAn > parDate.chAn)
		{
			return 1 ;
		}
		
		if (chMois < parDate.chMois)
		{
			return -1 ;
		}
		else if (chMois > parDate.chMois)
		{
			return 1 ;
		}
		
		if (chJour < parDate.chJour)
		{
			return -1 ;
		}
		else if (chJour > parDate.chJour)
		{
			return 1 ;
		}
		
		return 0 ;
	}

	/*public Date dateDuLendemain(Date parDate) // methode
	{
		chJour++ ;
		if (chJour > dernierJourDuMois() ) 
		{
			chMois++ ;
			chJour = 1 ;
		}
		if (chMois > 12) 
		{
			chAn++ ;
			chMois = 1 ;
		}
		return new Date(chJour,chMois,chAn) ;
	}

	public Date dateDeLaVeille(Date parDate) // methode
	{
		chJour-- ;
		if (chJour < 1) 
		{
			chMois-- ;
			chJour = dernierJourDuMois() ;
		}
		if (chMois < 1) 
		{
			chJour = 31 ;
			chMois = 12 ;
			chAn-- ;
		}
		return new Date(chJour,chMois,chAn) ;
	}*/
	
	public int getJour()
	{
		return chJour ;
	}
	
	public int getMois()
	{
		return chMois ;
	}
	
	public int getAn()
	{
		return chAn ;
	}
	
	public void setJour(int parJour)
	{
		chJour = parJour ;
	}
	public void setMois(int parMois)
	{
		chMois = parMois ;
	}
	public void setAn(int parAn)
	{
		chAn = parAn ;
	}
} // classe Date