package Vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.TitledBorder;

import Modele.*;

public class PanelEvenement extends JPanel implements ActionListener {

	Date chDate = new Date();
	Agenda chAgenda;
	
	JLabel chLabDate = new JLabel(chDate.toString());
	JButton ajouter = new JButton("+");

	JTextField titre = new JTextField();
	JTextField lieu = new JTextField();
	JTextArea description = new JTextArea(3,4);
	JTextArea afficherEvt = new JTextArea(7,16);
	
	JLabel labTitre = new JLabel("Titre");
	JLabel labLieu = new JLabel("Lieu");
	JLabel labDebut = new JLabel("Debut");
	JLabel labFin = new JLabel("Fin");
	JLabel labDate = new JLabel();
	JLabel labDescription = new JLabel("Description");
	String [] tabheure = new String[60];
	JComboBox debut1 ;
	JComboBox debut2 ;
	JComboBox fin1 ;
	JComboBox fin2 ;
	
	public PanelEvenement(Agenda parAgenda){
		chAgenda= parAgenda;
		
		this.setLayout(new GridBagLayout());
		
		
		
		for (int i = 0 ; i < 60 ; i++)
			tabheure[i] = Integer.toString(i) ;
		
		debut1 = new JComboBox(tabheure);
		debut2 = new JComboBox(tabheure);
		fin1 = new JComboBox(tabheure);
		fin2  = new JComboBox(tabheure);
			
		 
		 
		GridBagConstraints contrainte = new GridBagConstraints();
		///////////////////////////////////////////////////////
		contrainte.gridx=0;
		contrainte.gridy=0;
		contrainte.gridwidth=3;
		contrainte.fill = GridBagConstraints.BOTH;contrainte.insets = new Insets(10,10,10,10);
		//////////////////////////////////////////////////////////
		
		this.add(chLabDate,contrainte);
		
		contrainte.gridx=3;
		contrainte.gridy=0;
		contrainte.gridwidth=1;
		
		this.add(ajouter,contrainte);
		
		/////////////////////////////////////////////////////////////////////////
		contrainte.gridx=0;
		contrainte.gridy=1;
		contrainte.gridwidth=1;
		
		this.add(labTitre,contrainte);
		
		
		///////////////////////////////////////////////////////////
		
		contrainte.gridx=2;
		contrainte.gridy=1;
		contrainte.gridwidth=3;
		this.add(titre,contrainte);
		
		//////////////////////////////////////////////////////////
		contrainte.gridx=0;
		contrainte.gridy=2;
		contrainte.gridwidth=1;
		this.add(labLieu,contrainte);
		/////////////////////////////////////////////////////////////
		contrainte.gridx=2;
		contrainte.gridy=2;
		contrainte.gridwidth=3;
		this.add(lieu,contrainte);
		///////////////////////////////////////////////////////
		
		contrainte.gridx=0;
		contrainte.gridy=3;
		contrainte.gridwidth=1;
		this.add(labDebut,contrainte);
		///////////////////////////////////////////////////////
		contrainte.gridx=2;
		contrainte.gridy=3;
		contrainte.gridwidth=1;
		this.add(debut1,contrainte);
		
		///////////////////////////////////////////////////////
		contrainte.gridx=3;
		contrainte.gridy=3;
		contrainte.gridwidth=1;
		this.add(debut2,contrainte);
		
		//////////////////////////////////////////////////////
		
		contrainte.gridx=0;
		contrainte.gridy=4;
		contrainte.gridwidth=1;
		this.add(labFin,contrainte);
		////////////////////////////////////////////////////
		
		contrainte.gridx=2;
		contrainte.gridy=4;
		contrainte.gridwidth=1;
		this.add(fin1,contrainte);
		
		///////////////////////////////////////////////////////
		contrainte.gridx=3;
		contrainte.gridy=4;
		contrainte.gridwidth=1;
		this.add(fin2,contrainte);
		
		
		///////////////////////////////////////////////////////
		
		contrainte.gridx=0;
		contrainte.gridy=5;
		contrainte.gridwidth=2;
		this.add(labDescription,contrainte);
		//////////////////////////////////////////////////////
		contrainte.gridx=0;
		contrainte.gridy=6;
		contrainte.gridwidth=4;
		this.add(description,contrainte);
	
		//////////////////////////////////////////////////////
		contrainte.gridx=6;
		contrainte.gridy=0;
		contrainte.gridwidth=16;
		contrainte.gridheight=7;
		this.add(afficherEvt,contrainte);
		afficherEvt.setBorder(new TitledBorder("Evenement"));
		
		ajouter.addActionListener(this);
}
	

	public void actionPerformed(ActionEvent parEvt){
		if(parEvt.getSource()==ajouter){
			String str = titre.getText() + lieu.getText();
			Evt evt = new Evt(chDate,titre.getText(),lieu.getText(),(String) debut1.getSelectedItem(),(String) debut2.getSelectedItem(),(String) fin1.getSelectedItem(),(String) fin2.getSelectedItem());
			chAgenda.ajoutEvt(chDate.toString(),evt);
			afficherEvt.setText(chAgenda.toString());

		}
	}	
}
	



