package Modele;


import java.util.*;

public class  Agenda /*implements Comparable<Evt>*/
{
	private Evt[] chEvts ;
	//private ArrayList <Evt> chList ;
	private TreeSet <Evt> chSet ;
	private static final int chCapacite = 100 ; //constante
	private int chNbEvt ;
	private HashMap <String, ArrayList> chMap = new HashMap <String, ArrayList> () ;
	
	public Agenda()
	{
		//chEvts = new Evt[chCapacite] ;		//Tableau
		//chList = new ArrayList<Evt>(chCapacite) ;	//ArrayList 
		chSet = new TreeSet<Evt>() ;
		chNbEvt = 0 ; 
	}
	

	
	public void ajoutEvt(Evt parEvt)
	{
		if (chNbEvt < chCapacite)
		{
			chEvts[chNbEvt] = parEvt ; //Tableau
			chNbEvt++ ;
		}
	}
	
	public String toString()
	{
		/*/*String chaine = new String() ;
		for (int i = 0 ; i < chNbEvt ; i++)
			chaine += chEvts[i].toString() + "\n" ;
		return chaine ;*/
		
		/*return chMap + "\n" ;*/
		
		Set <String> cles = chMap.keySet() ;
		String str = new String();
		for (String cle : cles)
		{
			str += cle + " \n ";
			ArrayList liste = (ArrayList) chMap.get(cle);
			for (int i = 0; i < liste.size(); i++)
				str += liste.get(i) + "\n";
		}
		return str ;
	}
	
	
	/*public int rechercheMinimum(int parDebut, int parFin)
	{
		int chIndiceMini = parDebut ;
		Evt eMin = chEvts[parDebut];
		for (int i = parDebut+1 ; i <= parFin ; i++)
		{
			if (chEvts[i].compareTo(eMin) == -1)
			{
				chIndiceMini = i ;
				eMin = chEvts[i] ;
			}
		}
		return chIndiceMini ;
	}
	
	public void triCroissant() 
	{
		Evt tampon = new Evt(null,null,null) ;
		for (int i = 0 ; i < chNbEvt ; i++)
		{
			int iMin = rechercheMinimum(i,chNbEvt-1) ;
			tampon = chEvts[i] ;
			chEvts[i] = chEvts[iMin] ;
			chEvts[iMin] = tampon ;
			
		}

	}*/
	
	public void ajout(Evt parEvt)
	{
		chSet.add(parEvt) ;
		/*chList.add(parEvt) ;*/
	}



	public int compareTo(Evt arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/*public int calNbEvt(String parEvt)
	{
		int cptr = 0 ;
		for (int i = 0; i < chSet.size(); i++) 
		{
			if (chSet.get(i).getTitre() == parEvt)
				cptr += 1 ;
		}
		return cptr ;
	}
	
	public int calNbEvt2(String parEvt)
	{
		Iterator iList = chList.iterator() ;
		int cptr = 0 ;
		while (iList.hasNext() )
		{
			Evt e = iList.next() ;
			if (e.getTitre() == parEvt)
				cptr++ ;
		}
		return cptr ;
	}
	
	public int calNbEvt(Date parDate)
	{
		Iterator it = chSet.iterator() ;
		int cptr = 0 ;
		while (it.hasNext() )
		{
			Evt e = it.next() ;
			if (parDate.compareTo(e.getDate() ) == 0)
				cptr++ ;
		}
		return cptr ;
	}*/
	
	public void ajoutEvt(String parCle, Evt parEvt)
	{
		if (chMap.containsKey(parCle) == true)
		{
			ArrayList list = chMap.get(parCle);
			list.add(parEvt);
		}
		else
		{
			ArrayList al = new ArrayList <Evt>() ;
			al.add(parEvt);
			chMap.put(parCle, al) ;
		}
	}
	
}