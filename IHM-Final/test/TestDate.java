import static org.junit.Assert.*;

import org.junit.Test;

import Modele.Date;


public class TestDate {

	@Test
	public void test() {
		assertTrue (new Date(1, 1, 2016).estValide());
		//assertTrue(new Date(34, 15, 2016).estValide());
		assertEquals(31, Date.dernierJourDuMois(5,2017));

	}

}
